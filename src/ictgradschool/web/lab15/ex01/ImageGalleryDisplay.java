package ictgradschool.web.lab15.ex01;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by qpen546 on 10/05/2017.
 */
public class ImageGalleryDisplay extends HttpServlet {


    public ImageGalleryDisplay() {
        super();

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //response.getWriter().append("Served at: ").append(request.getContextPath());
        //displayBasic(request, response);
        displayHTML(request, response);
    }

    public void displayHTML(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");

        File folder = new File(fullPhotoPath);
        File[] fileList = folder.listFiles();
        Map<String, String[]> fileInfo = new TreeMap<>();

        try {
            for (File file : fileList) {
                String fileName = file.getName();
                if (fileName.contains("_thumbnail.png")) {
                    String fileAddress = "/Photos/" + fileName;
                    fileName = fileName.replace("_thumbnail.png", "");
                    fileName = fileName.replaceAll("_", " ");
                    String fullSizeFileAddress = fileAddress.replace("_thumbnail.png", ".jpg");
                    String fileFullSize = String.valueOf(new File(file.getAbsolutePath().replace("_thumbnail.png", ".jpg")).length());
                    String[] fileDetails = {fileAddress, fullSizeFileAddress, fileFullSize};
                    fileInfo.put(fileName, fileDetails);
                }
            }

            PrintWriter out = response.getWriter();

            out.println("<html>\n<head><title>Server response</title>");
            out.println("</head>\n<body>");
            out.println("<div>\n<table style=\"border: solid black 1px\">");
            out.println("<tr style=\"border: solid black 1px\">");
            out.println("<th style=\"border: solid black 1px\">Thumbnail</th>");
            out.println("<th style=\"border: solid black 1px\">Filename</th>");
            out.println("<th style=\"border: solid black 1px\">File-size</th>");
            out.println("</tr>");
            for (String key : fileInfo.keySet()) {
                out.println("<tr style=\"border: solid black 1px\">");
                out.println("<td style=\"border: solid black 1px\"><a href=\"" + fileInfo.get(key)[1] + "\"><img src=\"" + fileInfo.get(key)[0] + "\"></a></td>");
                out.println("<td style=\"border: solid black 1px\">" + key + "</td>");
                out.println("<td style=\"border: solid black 1px\">" + fileInfo.get(key)[2] + "</td>");
                out.println("</tr>");
            }
            out.println("</table>\n</div>\n</body>\n</html>");
        }catch(IOException e){
            e.getMessage();
        }


    }


}
